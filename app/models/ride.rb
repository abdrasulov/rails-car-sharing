class Ride < ActiveRecord::Base
  belongs_to :departure_place, class_name: 'Place', foreign_key: 'departure_place_id'
  belongs_to :arrival_place, class_name: 'Place', foreign_key: 'arrival_place_id'

  validates :departure_place,
            :arrival_place,
            :price,
            :date,
            :contact_person_name,
            :contact_phone_number,
            :car_make_model,
            :car_seats,
            presence: true

  default_scope {order(:date)}
  scope :actual, -> {where(['date > ?', DateTime.now])}

  def self.permit_params
    [:departure_place_name,
     :arrival_place_name,
     :price,
     :date,
     :contact_person_name,
     :contact_phone_number,
     :car_make_model,
     :car_seats]
  end

  def departure_place_name=(name)
    self.departure_place = Place.find_or_create_by(name: name) unless name.blank?
  end

  def departure_place_name
    departure_place.name if departure_place
  end

  def arrival_place_name=(name)
    self.arrival_place = Place.find_or_create_by(name: name) unless name.blank?
  end

  def arrival_place_name
    arrival_place.name if arrival_place
  end

end
