class RidesController < ApplicationController
  def index
    @ride_search = Ride.actual.includes(:departure_place, :arrival_place).ransack(params[:q])
    @rides = @ride_search.result.page(params[:page]).per(10)
  end

  def show
    @ride = Ride.find(params[:id])
  end

  def new
    @ride = Ride.new
  end

  def create
    @ride = Ride.new(permit_params)
    if @ride.save
      redirect_to rides_path
    else
      render :new
    end
  end

  private

    def permit_params
      params.require(:ride).permit(Ride.permit_params)
    end
end
