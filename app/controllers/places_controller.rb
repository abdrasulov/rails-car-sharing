class PlacesController < ApplicationController
  def selected
    respond_to do |format|
      place = Place.find_by_place_id params[:place][:place_id]
      place ||= Place.new(place_params)
      place.save if place.new_record?
      format.json {render json: 'ok'.to_json }
    end
  end

  private

    def place_params
      params[:place].permit(:name, :place_id, :vicinity, :lat, :lon)
    end
end
