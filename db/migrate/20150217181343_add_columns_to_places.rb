class AddColumnsToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :place_id, :string
    add_column :places, :vicinity, :string
    add_column :places, :lat, :float
    add_column :places, :lon, :float
  end
end
