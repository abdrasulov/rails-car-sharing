class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.references :departure_place, index: true
      t.references :arrival_place, index: true
      t.datetime :date

      t.timestamps null: false
    end
    add_foreign_key :rides, :places, column: :departure_place_id
    add_foreign_key :rides, :places, column: :arrival_place_id
  end
end
