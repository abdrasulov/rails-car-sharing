class AddCarSeatsToRides < ActiveRecord::Migration
  def change
    add_column :rides, :car_seats, :integer
  end
end
