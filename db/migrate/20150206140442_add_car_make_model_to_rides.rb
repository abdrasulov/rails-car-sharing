class AddCarMakeModelToRides < ActiveRecord::Migration
  def change
    add_column :rides, :car_make_model, :string
  end
end
