class AddContactToRides < ActiveRecord::Migration
  def change
    add_column :rides, :contact_person_name, :string
    add_column :rides, :contact_phone_number, :string
  end
end
