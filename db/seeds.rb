Ride.destroy_all
Place.destroy_all

places = [
    {place_id: "ChIJgcizkdy3njgRfeLefPW6Lkk", vicinity: "Бишкек", name: "Бишкек", lat: 42.874722, lon: 74.61222199999997},
    {place_id: "ChIJd6Jm6ockmzgR6Z0NjISZB74", vicinity: "Балыкчи", name: "Балыкчи, Иссык-Кульская область", lat: 42.461111, lon: 76.18027800000004},
    {place_id: "ChIJw4Pb6GC8mTgR1WAYSmAmLm0", vicinity: "Чаек", name: "Чаек, Нарынская область", lat: 41.93, lon: 74.50999999999999},
    {place_id: "ChIJaxVD-T7xhDgR2NCAw8l_krk", vicinity: "Чолпон-Ата", name: "Чолпон-Ата, Иссык-Кульская область", lat: 42.65, lon: 77.08333300000004},
    {place_id: "ChIJhcUzJtEVvTgRjqvq8dJJKHk", vicinity: "Жалалабат", name: "Жалалабат, Джалалабад", lat: 40.933333, lon: 73}
]
places.each do |p|
  Place.create! p
end

100.times do
  p = Place.pluck(:id).sort{ rand - 0.5 }[0..1]
  Ride.create!(
      departure_place_id: p.first,
      arrival_place_id: p.last,
      price: rand(400) + 100,
      contact_person_name: Faker::Name.name,
      contact_phone_number: Faker::PhoneNumber.cell_phone,
      car_make_model: 'BMW X5',
      car_seats: rand(2) + 1,
      date: Faker::Time.between(5.days.ago, 5.days.from_now, :day)
  )
end
AdminUser.create!(email: 'admin@example.com', password: 'password') if AdminUser.count.zero?