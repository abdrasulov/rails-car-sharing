Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :rides, except: [:edit, :update, :destroy]

  post '/places/selected', to: 'places#selected', as: :place_selected

  root to: 'rides#index'
end
